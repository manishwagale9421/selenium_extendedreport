package extentreport;

import java.util.concurrent.TimeUnit;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestClassUsingListener {
	
	public WebDriver driver;
	
	@BeforeClass
	public void beforeClass() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://opensource-demo.orangehrmlive.com/");
    }
	
	@Test
	public void testSuccessful() {
		System.out.println("Executing Sucessful Test Methods");
	}
	
	@Test
	public void testFailed() {
		System.out.println("Executing Failed Test Methods");
		Assert.fail("Executing Failed Test");
	}
	
	@Test
	public void testSkipped() {
		System.out.println("Executing Skipped Test Methods");
		throw new SkipException("Executing Skipped Test");
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}


}
