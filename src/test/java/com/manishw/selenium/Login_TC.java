package com.manishw.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Login_TC {

	@Test
	public void login_test()
	{
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("http://18.206.171.81:8080/Webapp/");
		
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("Admin");
		driver.findElement(By.xpath(" //input[@id='password']")).sendKeys("admin123");
		driver.findElement(By.xpath("//input[@id='submit']")).click();
		driver.findElement(By.xpath("//body/div[1]/div[1]/a[3]")).click();
		
		
		Assert.assertEquals(driver.getTitle(),"W3.CSS Template");
		
		driver.close();
		
	}
}
